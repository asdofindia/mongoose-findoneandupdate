const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useFindAndModify: false});

const demo = async () => {
    const Cat = mongoose.model('Cat', { name: String, position: Number });

    const kitty = new Cat({ name: 'Zildjian', position: 1 });
    await kitty.save()
    console.log(`meow saved at position ${kitty.position}`);

    const filter = { name: 'Zildjian' };
    const update = { "$inc": { position: 1  }  };

    const promotedKitty = await Cat.findOneAndUpdate(filter, update, {new: true})
    console.log(`meow being heard from promoted position ${promotedKitty.position}`);
}

demo().then(mongoose.disconnect)
